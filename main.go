package main

import (
	"context"
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"sort"
	"strconv"
	"time"

	"github.com/mattn/go-mastodon"
)

const sleepSecsBetweenCalls = 0

var configPtr = flag.String("config", "config.json", "path to config.json")
var followersPtr = flag.String("followers", "followers.json", "path to followers.json")
var dummyConfigPtr = flag.Bool("dummy", false, "create a dummy config, can be used together with config flag")
var lastDaysPtr = flag.Int("lastdays", 10, "list unfollowers within the last .. days")

// Watcher represents a Mastodon follower
type Watcher struct {
	ID           mastodon.ID `json:"id"`
	Acct         string      `json:"acct"`
	FollowedAt   time.Time   `json:"followed_at"`
	UnfollowedAt time.Time   `json:"unfollowed_at"`
}

// Config includes all parameters to connect to a Mastodon instance
type Config struct {
	Server           string
	ClientID         string
	ClientSecret     string
	MastodonAccount  string
	MastodonPasswort string
}

func saveFollowers(followers []*Watcher, path string) {
	jsonFollowers, err := json.MarshalIndent(followers, "", "  ")
	if err != nil {
		log.Println("saveFollowers json marshal error")
		log.Fatal(err)
	}
	//log.Println(string(jsonFollowers))
	err = ioutil.WriteFile(path, jsonFollowers, 0644)
	if err != nil {
		log.Println("saveFollowers write error")
		log.Fatal(err)
	}
}

func loadFollowers(path string) (followers []*Watcher) {
	jsonFile, err := os.Open(path)
	if err != nil {
		fmt.Println(err)
	}
	defer jsonFile.Close()
	byteFollowers, _ := ioutil.ReadAll(jsonFile)
	json.Unmarshal(byteFollowers, &followers)
	return followers
}

// returns true if follower is in history AND has never unfollowed us before
// otherwise it returns false. This means it returns false in case
//    * the follower is not in the history
// OR * the follower is in the history, but has unfollowed in past
func isFollowerKnown(followers []*Watcher, newF *Watcher) (isKnown bool) {
	for i := range followers {
		if followers[i].ID == newF.ID {
			// treat historic followers who have unfollowed as unknown:
			if followers[i].UnfollowedAt.Equal(time.Time{}) {
				return true
			}
			// although the ID is the same, the account unfollowed us in the past
			// so this account is not known to us because it is new (old) follower
			log.Println("found a re-follower:", newF.Acct)
			return false
		}
	}
	return false
}

// find the account f in the followers and update the FollowedAt field of the found account in the list
func updateFollowDate(followers []*Watcher, f *Watcher) {
	for i := range followers {
		if followers[i].ID == f.ID {
			followers[i].FollowedAt = f.FollowedAt
		}
	}
}

func matchFollowers(currentWatchers []*Watcher, historicFollowers []*Watcher, lastDays int) (comparedFollowers []*Watcher, newFollowers []*Watcher, newUnfollowers []*Watcher) {
	// copy current followers to match and recognize new followers
	for i := range currentWatchers {
		comparedFollowers = append(comparedFollowers, currentWatchers[i])
		isKnown := isFollowerKnown(historicFollowers, currentWatchers[i])
		if isKnown {
			// we already know this account is following us and has never unfollowed us before
		} else {
			// newFollowers saves explicit new followers and followers who have unfollowed us before
			newFollowers = append(newFollowers, currentWatchers[i])
		}
	}

	// prepare times
	currentTime := time.Now()
	lastDaysAsDuration, err := time.ParseDuration(strconv.Itoa(lastDays*24) + "h")
	if err != nil {
		log.Fatal(err)
	}
	// add historic followers missing in current followers list to the match
	for i := range historicFollowers {
		isKnown := isFollowerKnown(currentWatchers, historicFollowers[i])
		if isKnown {
			// historic follower is still in the current list, so we have to update the FollowedAt time to the original follow time
			updateFollowDate(currentWatchers, historicFollowers[i])
		} else {
			// historic follower is not part of the current follower list anymore
			nullTime := time.Time{}
			if historicFollowers[i].UnfollowedAt == nullTime {
				// account is following in the past, but not in present
				// update unfollow time and save the new unfollow to a list
				historicFollowers[i].UnfollowedAt = currentTime
				newUnfollowers = append(newUnfollowers, historicFollowers[i])
			} else {
				// account has unfollowed in past, print that info if it is in the range of lastDays
				if currentTime.Sub(historicFollowers[i].UnfollowedAt) <= lastDaysAsDuration {
					log.Printf("%v has already unfollowed within the last %d days: %v", historicFollowers[i].Acct, lastDays, historicFollowers[i].UnfollowedAt)
				}
			}
			// account is not known in current follower list, so finally add the historical account to the match list.
			// it is done at the end of this block, because potentially the UnfollowedAt has been updated just before
			comparedFollowers = append(comparedFollowers, historicFollowers[i])
		}

	}
	return comparedFollowers, newFollowers, newUnfollowers
}

func countCurrentWatchers(followers []*Watcher) int {
	c := 0
	for i := range followers {
		nullTime := time.Time{}
		if followers[i].UnfollowedAt == nullTime {
			c++
		}
	}
	return c
}

func readConfig(path string) (config *Config) {
	jsonFile, err := os.Open(path)
	if err != nil {
		log.Fatal(err)
	}
	defer jsonFile.Close()
	byteConfig, _ := ioutil.ReadAll(jsonFile)
	json.Unmarshal(byteConfig, &config)
	return config
}

func createDummyConfig(configPath string) {
	c := &Config{Server: "https://social.tchncs.de", ClientID: "132456", ClientSecret: "s0s3cr3t", MastodonAccount: "account@instan.ce", MastodonPasswort: "t0pS3cr3t"}
	jsonConfig, err := json.MarshalIndent(c, "", "  ")
	if err != nil {
		log.Println("createDummyConfig json marshal error")
		log.Fatal(err)
	}
	err = ioutil.WriteFile(configPath, jsonConfig, 0644)
	if err != nil {
		log.Println("createDummyConfig write error")
		log.Fatal(err)
	}
}

func handleFlags() {
	flag.Parse()
	// log.Println("config:", *configPtr)
	// log.Println("followers:", *followersPtr)
	// log.Println("dummy config:", *dummyConfigPtr)
	if *dummyConfigPtr {
		createDummyConfig(*configPtr)
		os.Exit(0)
	}
}

func printFollower(follower []*Watcher) {
	for i := range follower {
		log.Printf("\t\t@%v", follower[i].Acct)
	}
}

func main() {
	// init, load config, login to mastodon
	log.SetFlags(log.LstdFlags | log.Lshortfile)
	startTime := time.Now()
	log.Println(startTime)
	handleFlags()
	config := readConfig(*configPtr)
	c := mastodon.NewClient(&mastodon.Config{
		Server:       config.Server,
		ClientID:     config.ClientID,
		ClientSecret: config.ClientSecret,
	})
	err := c.Authenticate(context.Background(), config.MastodonAccount, config.MastodonPasswort)
	if err != nil {
		log.Println("auth error")
		log.Fatal(err)
	}

	// get my user account
	account, err := c.GetAccountCurrentUser(context.Background())
	if err != nil {
		log.Fatal(err)
	}
	// load all current followers via API
	var currentFollowers []*mastodon.Account
	var pg mastodon.Pagination
	for {
		// log.Println("Getting followers with pg.MaxID:", pg.MaxID)
		fs, err := c.GetAccountFollowers(context.Background(), account.ID, &pg)
		if err != nil {
			log.Fatal(err)
		}
		// log.Println("Number of new followers from this page:", len(fs))
		currentFollowers = append(currentFollowers, fs...)
		if pg.MaxID == "" || len(fs) == 0 {
			break
		}
		pg.SinceID = ""
		pg.MinID = ""
		// log.Println("Sleeping", sleepSecsBetweenCalls, "seconds, len(currentFollowers):", len(currentFollowers), "; pg.MaxID:", pg.MaxID)
		time.Sleep(sleepSecsBetweenCalls * time.Second)
	}

	var currentWatchers []*Watcher

	// convert API result to Watcher array
	for _, follower := range currentFollowers {
		//fmt.Printf("%v,%v\n", follower.ID, follower.Acct)
		newFollow := &Watcher{ID: follower.ID, Acct: follower.Acct, FollowedAt: time.Now(), UnfollowedAt: time.Time{}}
		currentWatchers = append(currentWatchers, newFollow)
	}

	historicFollowers := loadFollowers(*followersPtr)
	log.Printf("%v followers in total (incl. already unfollowed)", len(historicFollowers))
	log.Printf("%v followers before", countCurrentWatchers(historicFollowers))
	comparedFollowers, newFollowers, newUnfollowers := matchFollowers(currentWatchers, historicFollowers, *lastDaysPtr)
	log.Printf("%v followers after", countCurrentWatchers(comparedFollowers))

	// sort by ID before saving
	sort.Slice(comparedFollowers, func(i, j int) bool {
		first, err := strconv.Atoi(string(comparedFollowers[i].ID))
		if err != nil {
			log.Fatalln("cannot convert mastodon.ID to int:", err)
		}
		second, err := strconv.Atoi(string(comparedFollowers[j].ID))
		if err != nil {
			log.Fatalln("cannot convert mastodon.ID to int:", err)
		}
		return first < second
	})
	saveFollowers(comparedFollowers, *followersPtr)

	log.Printf("%v new followers", len(newFollowers))
	printFollower(newFollowers)
	log.Printf("%v new unfollowers", len(newUnfollowers))
	printFollower(newUnfollowers)
	log.Println(time.Now())
}
